package com.microservice.eureka.server.eurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.reactive.LoadBalancerExchangeFilterFunction;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.cloud.netflix.hystrix.HystrixCommands;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@EnableEurekaServer
@SpringBootApplication
public class EurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaServerApplication.class, args);
	}


	// configure reactive, Ribbon aware `WebClient`
	// API Adapter using reactive Spring WebFlux WebClient
//	@Bean
//	WebClient client(LoadBalancerExchangeFilterFunction lb) {
//		return WebClient.builder().filter(lb).build();
//	}
//	@Bean
//	RouterFunction<?> endpoints(WebClient client) {
//		List<String> badCars = Arrays.asList("test", "test");
//		return route(GET("/good-cars"), req -> {
//			Publisher<Car> beers = client
//					.get()
//					.uri("http://car-catalog-service/cars")
//					.retrieve()
//					.bodyToFlux(Car.class)
//					.filter(x -> badCars.contains(x.getName()));
//			Publisher<Car> circuit = HystrixCommands
//					.from(beers)
//					.commandName("beers")
//					.fallback(Flux.empty())
//					.eager()
//					.build();
//			return ServerResponse.ok().body(circuit, Car.class);
//		});
//	}
//	// API gateway with Spring Cloud Gateway
//	@Bean
//	RouteLocator gateway() {
//		return Routes.locator()
//				// custom paths to a load-balanced service
//				.route("path_route")
//				.predicate(path("/edge-results"))
//				.uri("lb://my-service/actual-results")
//				// rewrites
//				.route("rewrite_route")
//				.predicate(host("*.rewrite.org"))
//				.filter(rewritePath("/foo/(?<segment>.*)", "/${segment}"))
//				.uri("http://httpbin.org:80")
//				// circuit breaker
//				.route("hystrix_route")
//				.predicate(host("*.hystrix.org"))
//				.filter(hystrix("slowcmd"))
//				.uri("http://httpbin.org:80")
//				.build();
//	}


}
